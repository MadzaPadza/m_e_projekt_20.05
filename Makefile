main: main.o klient.o
	gcc -o main main.o klient.o -lm

main.o: main.c moduly.h
	gcc -c main.c

klient.o: klient.c
	gcc -c klient.c
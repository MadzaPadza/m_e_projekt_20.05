#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

int main()
{
    int choice = 0;
    char surname[30] = "";

    struct Client *head;
    head = (struct Client*)malloc(sizeof(struct Client*));
    strcpy(head->surname, "Pajor");
    head ->next = NULL;
    
    while(1)
    {
        while(choice > 5 || choice < 1)
        {
            printf("WYBIERZ: \n 1-DODAC NOWEGO KLIENTA \n 2-USUNAC ISTNIEJACEGO KLIENTA\n 3-WYSWIETLIC LICZBE KLIENTOW \n 4-WYSWIETLIC NAZWISKA KLIENTOW \n 5-ZAKONCZ \n");
            scanf("%d", &choice);
        }
        printf("%d", choice);
        switch(choice)
        {
            case 1: 
            printf("WYBRANO DODANIE NOWEGO KLIENTA\n PODAJ NAZWISKO: ");
            scanf("%s", surname);
            push_back(&head, surname);
            break;

            case 2:
            printf("\n WYBRANO USUNIECIE ISTNIEJACEGO KLIENTA");
            surname[0] = "\0";
            printf("NAZWISKO KLIENTA, KTOREGO CHCESZ USNAC: ");
            scanf("%s", surname);
            pop_by_surname(&head, surname);
            break;

            case 3:
            printf("\n LICZBA KLIENTOW WYNOSI: %d \n", list_size(head));
            break;

            case 4:
            printf("LISTA KLIENTOW: \n \n");
            show_list(head);
            break;
            
            case 5: 
            free(head);
            exit(1);
            break;
        }
        choice = 0;
    }
    free(head);
    return 0;
}